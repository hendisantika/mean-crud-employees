# MEAN CRUD Employee

### Run this project by this command :
1. `git clone https://gitlab.com/hendisantika/mean-crud-employees.git`
2. `cd frontend`
3. `npm install`
4. `npm run dev`
5. `cd backend`
6. `npm install`
7. `ng serve -o`

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Employee

![Add New Employee](img/add.png "Add New Employee")

List All Employees

![List All Employees](img/list.png "List All Employees")
